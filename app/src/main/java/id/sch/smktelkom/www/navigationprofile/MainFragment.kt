package id.sch.smktelkom.www.navigationprofile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_main.*
class MainFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_main, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        navigate_dest_bt.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.first_profile, null))
        super.onViewCreated(view, savedInstanceState)
    }

}