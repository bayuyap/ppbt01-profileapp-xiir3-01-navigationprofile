package id.sch.smktelkom.www.navigationprofile

import android.annotation.TargetApi
import android.app.Activity
import android.app.Fragment
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class ProfileList(private val context: Activity, internal var profiles: List<Profile>) : ArrayAdapter<Profile>(context, R.layout.profile_list,profiles) {
    @TargetApi(Build.VERSION_CODES.O)
    fun getView(inflater: LayoutInflater, position : Int, convertView : View?, parent : ViewGroup): View {

        val list = inflater.inflate(R.layout.profile_list,null,true)

        val id_profile = list.findViewById<TextView>(R.id.id)
        val name = list.findViewById<TextView>(R.id.nama)
        val ttl = list.findViewById<TextView>(R.id.ttl)
        val alamat = list.findViewById<TextView>(R.id.alamat)
        val kelas = list.findViewById<TextView>(R.id.kelas)

        val profile = profiles[position]
        id_profile.text = profile.id_profile.toString()
        name.text = profile.nama
        ttl.text = profile.ttl
        alamat.text = profile.alamat
        kelas.text = profile.kelas

        return list
    }
}