package id.sch.smktelkom.www.navigationprofile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation

class FlowStepFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        val step = arguments?.let {
            val safeArgs = FlowStepFragmentArgs.fromBundle(it)
            safeArgs.step
        }

        return when (step) {
            2 -> inflater.inflate(R.layout.fragment_profile2, container, false)
            else -> inflater.inflate(R.layout.fragment_profile1, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.next_button).setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.next_action)
        )
    }
}